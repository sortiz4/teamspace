"""
User Forms

For more information on Forms, see:
https://docs.djangoproject.com/en/1.10/topics/forms/

For more information on ModelForms, see:
https://docs.djangoproject.com/en/1.10/topics/forms/modelforms/
"""
from django import forms
from django.contrib.auth import authenticate
from django.contrib.auth import password_validation
from teamspace.models import Organization
from teamspace.models import OrganizationMembership
from teamspace.models import Team
from teamspace.models import TeamMembership
from teamspace.models import User


class UserLoginForm(forms.Form):

    class Meta:
        fields = ['email', 'password']

    email = forms.EmailField()
    password = forms.CharField(strip=False)
    error_messages = {
        'invalid_login': 'Your email or password are incorrect.',
        'inactive': 'You cannot log in at this time.',
    }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.user = None

    def clean(self):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')
        if email and password:
            self.user = authenticate(email=email, password=password)
            if self.user is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                )
            else:
                self.login_allowed(self.user)
        return self.cleaned_data

    def login_allowed(self, user):
        if not user.is_active:
            raise forms.ValidationError(
                self.error_messages['inactive'],
                code='inactive',
            )


class UserSignupForm(forms.ModelForm):

    class Meta:
        model = User
        fields = [
            'first_name',
            'last_name',
            'username',
            'email',
            'password_one',
            'password_two',
        ]
        error_messages = {
            'first_name': {
                'required': 'Please enter your first name.',
            },
            'last_name': {
                'required': 'Please enter your last name.',
            },
            'username': {
                'required': 'Please enter a username.',
                'unique': 'This username is taken.',
            },
            'email': {
                'required': 'Please enter your email address.',
                'unique': 'This email address is taken.',
            },
        }

    password_one = forms.CharField(strip=False)
    password_two = forms.CharField(strip=False)
    error_messages = {
        'password_mismatch': 'The two password fields did not match.',
    }

    def clean_password_two(self):
        # Make sure the passwords match
        password_one = self.cleaned_data.get('password_one')
        password_two = self.cleaned_data.get('password_two')

        if password_one and password_two and password_one != password_two:
            # Check if the password fields match
            raise forms.ValidationError(
                self.error_messages['password_mismatch'],
                code='password_mismatch',
            )

        # Validate the password
        password_validation.validate_password(password_two, self.instance)
        return password_two

    # Specialized save function
    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data['password_one'])
        if commit:
            user.save()
        return user


class AddMember(forms.Form):

    email = forms.EmailField()
    error_messages = {
        'not_found': "We couldn't find this user.",
        'org_member': 'This user is already a member of this organization.',
        'team_member': 'This user is already a member of this team.',
    }

    def __init__(self, *args, **kwargs):
        self.user = None
        self.group = kwargs.pop('group')
        super().__init__(*args, **kwargs)

    def clean(self):
        # Attempts to find the user
        email = self.cleaned_data.get('email')
        try:
            self.user = User.objects.get(email__exact=email)
        except User.DoesNotExist:
            raise forms.ValidationError(
                self.error_messages['not_found'],
                code='not_found',
            )

        # Make sure the group membership doesn't exist yet
        if isinstance(self.group, Organization):
            if (
                OrganizationMembership.objects
                    .filter(user=self.user, organization=self.group)
                    .exists()
            ):
                raise forms.ValidationError(
                    self.error_messages['org_member'],
                    code='org_member',
                )
        elif isinstance(self.group, Team):
            if (
                TeamMembership.objects
                    .filter(user=self.user, team=self.group)
                    .exists()
            ):
                raise forms.ValidationError(
                    self.error_messages['team_member'],
                    code='team_member',
                )
        return self.cleaned_data

    def add_member(self):
        # Create a membership record for the user
        if isinstance(self.group, Organization):
            OrganizationMembership.objects.create(
                user=self.user,
                organization=self.group,
            )
        elif isinstance(self.group, Team):
            TeamMembership.objects.create(
                user=self.user,
                team=self.group,
            )
