"""
Message Forms

For more information on Forms, see:
https://docs.djangoproject.com/en/1.10/topics/forms/

For more information on ModelForms, see:
https://docs.djangoproject.com/en/1.10/topics/forms/modelforms/
"""
from django import forms
from teamspace.models import Message


class MessageCreateForm(forms.ModelForm):

    class Meta:
        model = Message
        fields = ['message']

    def __init__(self, *args, **kwargs):
        self.owner = kwargs.pop('owner')
        self.parent = kwargs.pop('parent')
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        message = super().save(commit=False)
        message.owner = self.owner
        message.parent = self.parent
        if commit:
            message.save()
        return message
