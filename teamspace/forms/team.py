"""
Team Forms

For more information on Forms, see:
https://docs.djangoproject.com/en/1.10/topics/forms/

For more information on ModelForms, see:
https://docs.djangoproject.com/en/1.10/topics/forms/modelforms/
"""
from django import forms
from teamspace.models import Team
from teamspace.models import TeamMembership


class BaseTeamForm(forms.ModelForm):

    class Meta:
        model = Team
        fields = ['name', 'description']
        error_messages = {
            'name': {
                'required': 'Please enter your team name.',
                'unique': 'This team name is taken.',
            },
            'description': {
                'required': 'Please enter a team description.',
            },
        }


class TeamCreateForm(BaseTeamForm):

    def __init__(self, *args, **kwargs):
        self.owner = kwargs.pop('owner')
        self.parent = kwargs.pop('parent')
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        team = super().save(commit=False)
        team.owner = self.owner
        team.parent = self.parent
        if commit:
            team.save()
            TeamMembership.objects.create(team=team, user=team.owner)
        return team


class TeamEditForm(BaseTeamForm):
    pass
