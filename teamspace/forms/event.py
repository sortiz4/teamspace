"""
Event Forms

For more information on Forms, see:
https://docs.djangoproject.com/en/1.10/topics/forms/

For more information on ModelForms, see:
https://docs.djangoproject.com/en/1.10/topics/forms/modelforms/
"""
from django import forms
from teamspace.models import Event


class EventCreateForm(forms.ModelForm):

    class Meta:
        model = Event
        fields = ['title', 'description', 'start', 'end']
        error_messages = {
            'title': {
                'required': 'Please enter a title.',
            },
            'description': {
                'required': 'Please enter a short description.',
            },
        }

    def __init__(self, *args, **kwargs):
        self.owner = kwargs.pop('owner')
        self.parent = kwargs.pop('parent')
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        event = super().save(commit=False)
        event.owner = self.owner
        event.parent = self.parent
        if commit:
            event.save()
        return event
