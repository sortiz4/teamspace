"""
Organization Forms

For more information on Forms, see:
https://docs.djangoproject.com/en/1.10/topics/forms/

For more information on ModelForms, see:
https://docs.djangoproject.com/en/1.10/topics/forms/modelforms/
"""
from django import forms
from teamspace.models import Organization
from teamspace.models import OrganizationMembership


class BaseOrganizationForm(forms.ModelForm):

    class Meta:
        model = Organization
        fields = ['name', 'description', 'address', 'country', 'state']
        errors = {
            'name': {
                'required': 'Please enter the name of your organization.',
                'unique': 'This organization name is taken.',
            },
            'description': {
                'required': 'Please enter an organization description.',
            },
        }


class OrganizationCreateForm(BaseOrganizationForm):

    def __init__(self, *args, **kwargs):
        self.owner = kwargs.pop('owner')
        super().__init__(*args, **kwargs)

    def save(self, commit=True):
        organization = super().save(commit=False)
        organization.owner = self.owner
        if commit:
            organization.save()
            OrganizationMembership.objects.create(
                organization=organization,
                user=organization.owner,
            )
        return organization


class OrganizationEditForm(BaseOrganizationForm):
    pass
