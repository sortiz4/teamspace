"""
WSGI Configuration

For more information on WSGI, see:
https://docs.djangoproject.com/en/1.10/howto/deployment/wsgi/
"""
import os
from django.core import wsgi

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'core.settings')
application = wsgi.get_wsgi_application()
