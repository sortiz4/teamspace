from django.db import models


class Organization(models.Model):

    # Database fields
    owner = models.ForeignKey('teamspace.User', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=50000)
    address = models.CharField(blank=True, max_length=255)
    country = models.CharField(blank=True, max_length=2)
    state = models.CharField(blank=True, max_length=2)
    members = models.IntegerField(default=1)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    # Constants
    DEFER = ['description', 'address', 'country', 'state']

    def load_active_teams(self, user):
        self.active_teams = self.team_set.filter(teammembership__user=user)
