from django.db import models


class TeamMembership(models.Model):

    # Database fields
    user = models.ForeignKey('teamspace.User', on_delete=models.CASCADE)
    team = models.ForeignKey('teamspace.Team', on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
