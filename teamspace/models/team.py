from django.db import models


class Team(models.Model):

    # Database fields
    owner = models.ForeignKey(
        'teamspace.User',
        on_delete=models.CASCADE,
    )
    parent = models.ForeignKey(
        'teamspace.Organization',
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=100)
    description = models.TextField(max_length=50000)
    members = models.IntegerField(default=1)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)

    # Constants
    DEFER = ['description']
