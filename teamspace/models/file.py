from django.db import models


class File(models.Model):

    # Database fields
    owner = models.ForeignKey('teamspace.User', on_delete=models.CASCADE)
    parent = models.ForeignKey('teamspace.Team', on_delete=models.CASCADE)
    name = models.CharField(blank=False, max_length=255)
    location = models.CharField(blank=False, max_length=255)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
