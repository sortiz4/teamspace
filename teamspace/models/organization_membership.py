from django.db import models


class OrganizationMembership(models.Model):

    # Database fields
    user = models.ForeignKey(
        'teamspace.User',
        on_delete=models.CASCADE,
    )
    organization = models.ForeignKey(
        'teamspace.Organization',
        on_delete=models.CASCADE,
    )
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
