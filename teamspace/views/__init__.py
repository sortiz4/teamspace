from django.contrib import messages
from django.shortcuts import redirect


def error(request):
    if request.user.is_authenticated():
        messages.warning(request, 'The request could not be found.')
        return redirect('dashboard:home')
    return redirect('frontend:home')
