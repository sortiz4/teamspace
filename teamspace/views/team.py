"""
Team Controller

For more information on controllers (views), see:
https://docs.djangoproject.com/en/1.10/#the-view-layer

For the full list of requests and their responses, see:
https://docs.djangoproject.com/en/1.10/ref/request-response/
"""
import os
from django.conf import settings
from django.contrib import messages
from django.http import Http404
from django.shortcuts import redirect
from django.shortcuts import render
from django.views.static import serve
from teamspace.decorators import require_logged_in
from teamspace.decorators import require_team_member
from teamspace.decorators import require_team_owner
from teamspace.forms import AddMember
from teamspace.forms import EventCreateForm
from teamspace.forms import MessageCreateForm
from teamspace.forms import TeamEditForm
from teamspace.models import Event
from teamspace.models import File
from teamspace.models import Message
from teamspace.models import Team
from teamspace.models import TeamMembership
from teamspace.utils import format_errors
from teamspace.views import google


@require_logged_in
@require_team_member
def leave(request, id):
    """
    Deletes the membership record if the user is a member.
    """
    try:
        team = Team.objects.get(pk=id)
    except Team.DoesNotExist:
        raise Http404

    try:
        membership = TeamMembership.objects.get(user=request.user, team=team)
    except TeamMembership.DoesNotExist:
        raise Http404

    if membership:
        # Delete the membership
        membership.delete()
        team.members -= 1
        team.save()
        messages.info(request, 'You have left the team.')

    # Redirect the user back to the team view
    return redirect('organization:team:view', team.parent_id)


@require_logged_in
@require_team_owner
def delete(request, id):
    """
    Deletes the team if the user is the owner.
    """
    try:
        team = Team.objects.get(pk=id)
    except Team.DoesNotExist:
        raise Http404

    if team.owner == request.user:
        # Make sure the user is the owner
        team.delete()
        messages.info(request, 'The team has been deleted.')

    # Redirect the user back to the team view
    return redirect('organization:team:view', team.parent_id)


@require_logged_in
@require_team_member
def home(request, id):
    """
    Returns a view with the team and a parsed markdown description.
    """
    try:
        team = Team.objects.get(pk=id)
    except Team.DoesNotExist:
        raise Http404

    # Render the template with the data set
    return render(
        request,
        'backend/team/home.html',
        {'team': team, 'organization': team.parent},
    )


@require_logged_in
@require_team_member
def events(request, id):
    """
    Returns a view with the team and a list of events.
    """
    try:
        team = Team.objects.defer(*Team.DEFER).get(pk=id)
    except Team.DoesNotExist:
        raise Http404
    form = None

    if request.method == 'POST':
        # Process the post data in the form
        form = EventCreateForm(request.POST, owner=request.user, parent=team)

        if form.is_valid():
            # Check if the form is valid
            form.save()
            form = None
        else:
            messages.error(request, format_errors(form.errors))

    # Fetch the event list
    events = (
        Event.objects
            .select_related('owner')
            .filter(parent=team)
            .order_by('-start')
    )

    if not events:
        messages.info(request, 'There are no events to display.')

    # Render the template with the data set
    return render(
        request,
        'backend/team/events.html',
        {
            'team': team,
            'organization': team.parent,
            'form': form,
            'events': events,
        },
    )


@require_logged_in
@require_team_member
def files(request, id):
    """
    Returns a view with a file manager window of the team.
    """
    try:
        team = Team.objects.defer(*Team.DEFER).get(pk=id)
    except Team.DoesNotExist:
        raise Http404

    if request.method == 'GET' and 'file_id' in request.GET:
        try:
            # This does not scale well
            file_id = int(request.GET['file_id'])
            file = File.objects.get(pk=file_id)
            if file.parent == team:
                return serve(
                    request,
                    os.path.basename(file.location),
                    os.path.dirname(file.location),
                )
            else:
                messages.error(request, 'This file could not be found.')
        except ValueError:
            messages.error(request, 'Invalid file.')

    if request.method == 'POST':
        # Upload the requested file
        return redirect(
            google.authorize(
                request,
                {'team_id': team.id, 'file_id': request.POST['file_id']},
            )
        )

    # Fetch the file list
    files = (
        File.objects
            .select_related('owner')
            .filter(parent=team)
            .order_by('-created')
    )

    if not files:
        messages.info(request, 'There are no files to display. Share some!')

    # Render the template with the data set
    return render(
        request,
        'backend/team/files.html',
        {
            'team': team,
            'files': files,
            'organization': team.parent,
            'google': settings.PROVIDERS['GOOGLE'],
        },
    )


@require_logged_in
@require_team_member
def chat(request, id):
    """
    Returns a view with a chat window for the team.
    """
    try:
        team = Team.objects.defer(*Team.DEFER).get(pk=id)
    except Team.DoesNotExist:
        raise Http404
    form = None

    if request.method == 'POST':
        # Process the post data in the form
        form = MessageCreateForm(request.POST, owner=request.user, parent=team)

        if form.is_valid():
            # Check if the form is valid
            form.save()
            form = None
        else:
            messages.error(request, 'Your message could not be posted.')

    # Fetch the message list
    team_messages = (
        Message.objects
            .select_related('owner')
            .filter(parent=team)
            .order_by('-created')
    )

    if not team_messages:
        messages.info(request, 'There are no messages to display.')

    # Render the template with the data set
    return render(
        request,
        'backend/team/chat.html',
        {
            'team': team,
            'organization': team.parent,
            'form': form,
            'team_messages': team_messages,
        },
    )


@require_logged_in
@require_team_member
def users(request, id):
    """
    Returns a view with a list of members in the team.
    """
    try:
        team = Team.objects.defer(*Team.DEFER).get(pk=id)
    except Team.DoesNotExist:
        raise Http404

    # Fetch a list of team members
    users = TeamMembership.objects.select_related('user').filter(team=team)

    # Render the template with the data set
    return render(
        request,
        'backend/team/users.html',
        {'team': team, 'users': users, 'organization': team.parent},
    )


@require_logged_in
@require_team_owner
def manage_edit(request, id):
    """
    Returns a view where an owner can edit their team.
    """
    try:
        team = Team.objects.defer(*Team.DEFER).get(pk=id)
    except Team.DoesNotExist:
        raise Http404

    if request.method == 'POST':
        # Update the form with the post data
        form = TeamEditForm(request.POST, instance=team)

        if form.is_valid():
            # Check if the form is valid
            form.save()
            messages.success(request, 'Settings successfully saved.')
        else:
            messages.error(request, format_errors(form.errors))
    else:
        # Initialize the default form
        form = TeamEditForm(instance=team)

    # Render the template with the data set
    return render(
        request,
        'backend/team/manage_edit.html',
        {'form': form, 'team': team, 'organization': team.parent},
    )


@require_logged_in
@require_team_owner
def manage_users(request, id):
    """
    Returns a view with a list of users and a form to add users.
    """
    try:
        team = Team.objects.defer(*Team.DEFER).get(pk=id)
    except Team.DoesNotExist:
        raise Http404
    form = None

    if request.method == 'POST':
        if 'email' in request.POST:
            # Handle invitations by creating a membership record
            form = AddMember(request.POST, group=team)

            if form.is_valid():
                # Check if the form is valid
                form.add_member()
                team.members += 1
                team.save()
                messages.success(request, 'User successfully added.')
            else:
                messages.error(request, format_errors(form.errors))

        if 'id' in request.POST:
            # Handle removals by deleting the membership record
            # `id` is not user controlled, so assume it's safe
            remove_user_id = int(request.POST['id'])

            try:
                # Find and delete the membership record
                (
                    TeamMembership.objects
                        .get(user_id=remove_user_id, team=team)
                        .delete()
                )
            except TeamMembership.DoesNotExist:
                raise Http404
            team.members -= 1
            team.save()

    # Get a list of team members
    users = TeamMembership.objects.select_related('user').filter(team=team)

    if team.members == 1:
        # Check if the member count only includes the owner
        messages.info(
            request,
            'Your team does not have any members. Invite some!',
        )

    # Render the template with the data set
    return render(
        request,
        'backend/team/manage_users.html',
        {
            'users': users,
            'form': form,
            'team': team,
            'organization': team.parent,
        },
    )
