"""
Organization Controller

For more information on controllers (views), see:
https://docs.djangoproject.com/en/1.10/#the-view-layer

For the full list of requests and their responses, see:
https://docs.djangoproject.com/en/1.10/ref/request-response/
"""
from django.contrib import messages
from django.http import Http404
from django.shortcuts import redirect
from django.shortcuts import render
from teamspace.decorators import require_logged_in
from teamspace.decorators import require_org_member
from teamspace.decorators import require_org_owner
from teamspace.forms import AddMember
from teamspace.forms import OrganizationEditForm
from teamspace.forms import TeamCreateForm
from teamspace.models import Organization
from teamspace.models import OrganizationMembership
from teamspace.models import Team
from teamspace.utils import format_errors


@require_logged_in
@require_org_member
def leave(request, id):
    """
    Deletes the membership record if the user is a member.
    """
    try:
        organization = Organization.objects.get(pk=id)
    except Organization.DoesNotExist:
        raise Http404
    try:
        membership = (
            OrganizationMembership.objects
                .get(user=request.user, organization=organization)
        )
    except OrganizationMembership.DoesNotExist:
        raise Http404

    if membership:
        # Delete the membership record
        membership.delete()
        organization.members -= 1
        organization.save()
        messages.info(request, 'You have left the organization.')

    # Redirect the user back to the organization view
    return redirect('dashboard:organization:view')


@require_logged_in
@require_org_owner
def delete(request, id):
    """
    Deletes the organization if the user is the owner.
    """
    try:
        organization = Organization.objects.get(pk=id)
    except Organization.DoesNotExist:
        raise Http404

    if organization.owner == request.user:
        # Make sure the user is the owner
        organization.delete()
        messages.info(request, 'The organization has been deleted.')

    # Redirect the user back to the organization view
    return redirect('dashboard:organization:view')


@require_logged_in
@require_org_member
def home(request, id):
    """
    Returns a view with the organization and a parsed markdown description.
    """
    try:
        organization = Organization.objects.get(pk=id)
    except Organization.DoesNotExist:
        raise Http404

    # Render the template with the data set
    return render(
        request,
        'backend/organization/home.html',
        {'organization': organization},
    )


@require_logged_in
@require_org_member
def users(request, id):
    """
    Returns a view containing a list of members in the organization.
    """
    try:
        organization = (
            Organization.objects
                .defer(*Organization.DEFER)
                .get(pk=id)
        )
    except Organization.DoesNotExist:
        raise Http404

    # Get a list of organization members
    users = (
        OrganizationMembership.objects
            .select_related('user')
            .filter(organization=organization)
    )

    # Render the template with the data set
    return render(
        request,
        'backend/organization/users.html',
        {'users': users, 'organization': organization},
    )


@require_logged_in
@require_org_member
def teams_view(request, id):
    """
    Returns a view containing a list of teams in the organization.
    """
    try:
        organization = (
            Organization.objects
                .defer(*Organization.DEFER)
                .get(pk=id)
        )
    except Organization.DoesNotExist:
        raise Http404

    # TODO: If the user is the organization owner, show all the teams.
    # TODO: If they are not the owner, only show teams where they are a member.

    # Fetch the corresponding list of teams
    teams = organization.team_set.defer(*Team.DEFER).all()

    if not teams:
        # Flash a message if the list of teams is empty
        messages.info(
            request,
            'Your organization does not have any teams. Go create one!',
        )

    # Render the template with the data set
    return render(
        request,
        'backend/organization/teams_view.html',
        {'teams': teams, 'organization': organization},
    )


@require_logged_in
@require_org_member
def teams_create(request, id):
    """
    Returns a view where users can create a team within the organization.
    """
    try:
        organization = (
            Organization.objects
                .defer(*Organization.DEFER)
                .get(pk=id)
        )
    except Organization.DoesNotExist:
        raise Http404

    if request.method == 'POST':
        # Process the post data in the form
        form = TeamCreateForm(
            request.POST,
            parent=organization,
            owner=request.user,
        )

        if form.is_valid():
            # Check if the form is valid
            team = form.save()
            messages.success(request, 'Team successfully created.')

            # Redirect to the newly created team
            return redirect('team:home', team.id)
        else:
            messages.error(request, format_errors(form.errors))

            # Return with the errors otherwise
            return render(
                request,
                'backend/organization/teams_create.html',
                {'form': form, 'organization': organization},
            )

    # Render the template with the data set
    return render(
        request,
        'backend/organization/teams_create.html',
        {'organization': organization},
    )


@require_logged_in
@require_org_owner
def manage_edit(request, id):
    """
    Returns a view where an owner can edit their organization.
    """
    try:
        organization = Organization.objects.get(pk=id)
    except Organization.DoesNotExist:
        raise Http404

    if request.method == 'POST':
        # Update the form with the post data
        form = OrganizationEditForm(request.POST, instance=organization)

        if form.is_valid():
            # Check if the form is valid
            form.save()
            messages.success(request, 'Settings successfully saved.')
        else:
            messages.error(request, format_errors(form.errors))
    else:
        # Initialize the default form
        form = OrganizationEditForm(instance=organization)

    # Render the template with the data set
    return render(
        request,
        'backend/organization/manage_edit.html',
        {'form': form, 'organization': organization},
    )


@require_logged_in
@require_org_owner
def manage_users(request, id):
    """
    Returns a view with a list of users and a form to add users.
    """
    try:
        organization = (
            Organization.objects
                .defer(*Organization.DEFER)
                .get(pk=id)
        )
    except Organization.DoesNotExist:
        raise Http404
    form = None

    if request.method == 'POST':
        if 'email' in request.POST:
            # Handle invitations by creating a membership record
            form = AddMember(request.POST, group=organization)

            if form.is_valid():
                # Check if the form is valid
                form.add_member()
                organization.members += 1
                organization.save()
                messages.success(request, 'User successfully added.')
            else:
                messages.error(request, format_errors(form.errors))

        if 'id' in request.POST:
            # Handle removals by deleting the membership record
            # `id` is not user controlled, so assume it's safe
            remove_user_id = int(request.POST['id'])

            try:
                # Find and delete the membership record
                (
                    OrganizationMembership.objects
                        .get(user_id=remove_user_id, organization=organization)
                        .delete()
                )
            except OrganizationMembership.DoesNotExist:
                raise Http404
            organization.members -= 1
            organization.save()

    # Get a list of organization members
    users = (
        OrganizationMembership.objects
            .select_related('user')
            .filter(organization=organization)
    )

    if organization.members == 1:
        # Check if the member count only includes the owner
        messages.info(
            request,
            'Your organization does not have any members. Invite some!',
        )

    # Render the template with the data set
    return render(
        request,
        'backend/organization/manage_users.html',
        {'users': users, 'form': form, 'organization': organization},
    )
