"""
Frontend Controller

For more information on controllers (views), see:
https://docs.djangoproject.com/en/1.10/#the-view-layer

For the full list of requests and their responses, see:
https://docs.djangoproject.com/en/1.10/ref/request-response/
"""
from django.contrib.auth import login as user_login
from django.contrib.auth import logout as user_logout
from django.http import JsonResponse
from django.shortcuts import redirect
from django.shortcuts import render
from teamspace.decorators import require_logged_out
from teamspace.forms import UserLoginForm
from teamspace.forms import UserSignupForm


@require_logged_out
def home(request):
    """
    Returns a basic view that acts as a starting point for Teamspace.
    """
    return render(request, 'frontend/home.html')


@require_logged_out
def about(request):
    """
    Returns a basic view containing information about Teamspace.
    """
    return render(request, 'frontend/about.html')


def logout(request):
    """
    Logs a user out and redirects them back home.
    """
    user_logout(request)
    return redirect('frontend:home')


def login(request):
    """
    This simple endpoint logs a user in.
    """
    if request.method == 'POST':
        # Process the post data in the form
        form = UserLoginForm(request.POST)

        if form.is_valid():
            # Check if the form is valid
            user = form.user
            user_login(request, user)
            return JsonResponse({'valid': True})
        else:
            # Respond with the errors otherwise
            return JsonResponse({'valid': False, 'errors': form.errors})

    # Redirect the user back home otherwise
    return redirect('frontend:home')


def signup(request):
    """
    This simple endpoint creates a user.
    """
    if request.method == 'POST':
        # Process the post data in the form
        form = UserSignupForm(request.POST)

        if form.is_valid():
            # Check if the form is valid
            user = form.save()
            user_login(request, user)
            return JsonResponse({'valid': True})
        else:
            # Respond with the errors otherwise
            return JsonResponse({'valid': False, 'errors': form.errors})

    # Redirect the user back home otherwise
    return redirect('frontend:home')
