"""
Dashboard Controller

For more information on controllers (views), see:
https://docs.djangoproject.com/en/1.10/#the-view-layer

For the full list of requests and their responses, see:
https://docs.djangoproject.com/en/1.10/ref/request-response/
"""
from django.contrib import messages
from django.shortcuts import redirect
from django.shortcuts import render
from teamspace.decorators import require_logged_in
from teamspace.forms import OrganizationCreateForm
from teamspace.models import Organization
from teamspace.utils import format_errors


@require_logged_in
def home(request):
    """
    Returns a view that acts as a starting point for the user.
    """
    return render(request, 'backend/dashboard/home.html')


@require_logged_in
def user_edit(request):
    """
    Returns a view with a form for the user to edit their account.
    """
    return render(request, 'backend/dashboard/user_edit.html')


@require_logged_in
def user_password(request):
    """
    Returns a view with a form for the user to change their password.
    """
    return render(request, 'backend/dashboard/user_password.html')


@require_logged_in
def orgs_view(request):
    """
    Returns a view containing a list of organizations the user is a member of.
    """
    organizations = (
        Organization.objects
            .defer(*Organization.DEFER)
            .filter(organizationmembership__user=request.user)
    )

    for organization in organizations:
        # Fetch a list of active teams for each organization
        organization.load_active_teams(request.user)

    if not organizations:
        # Flash a message if the list of teams is empty
        messages.info(
            request,
            "You're not a member of any organization yet. Go create one!",
        )

    # Render the template with the data set
    return render(
        request,
        'backend/dashboard/organizations_view.html',
        {'organizations': organizations},
    )


@require_logged_in
def orgs_create(request):
    """
    Returns a view where users can create an organization.
    """
    if request.method == 'POST':
        # Process the post data in the form
        form = OrganizationCreateForm(request.POST, owner=request.user)

        if form.is_valid():
            # Check if the form is valid
            organization = form.save()
            messages.success(request, 'Organization successfully created.')

            # Redirect to the newly created team
            return redirect('organization:home', organization.id)
        else:
            messages.error(request, format_errors(form.errors))

            # Return with the errors otherwise
            return render(
                request,
                'backend/dashboard/organizations_create.html',
                {'form': form},
            )

    # Render the creation template
    return render(request, 'backend/dashboard/organizations_create.html')
