"""
Google Controller

For more information on controllers (views), see:
https://docs.djangoproject.com/en/1.10/#the-view-layer

For the full list of requests and their responses, see:
https://docs.djangoproject.com/en/1.10/ref/request-response/
"""
import httplib2
import json
import os
from apiclient import discovery
from apiclient import errors
from apiclient import http
from django.conf import settings
from django.core.cache import cache
from django.http import Http404
from django.shortcuts import redirect
from django.urls import reverse
from oauth2client import client
from teamspace.decorators import require_logged_in
from teamspace.models import File

CLIENT_SECRETS = os.path.join(settings.BASE_DIR, 'conf', 'google.json')
SCOPES = 'https://www.googleapis.com/auth/drive/'


def cache_key(request):
    """
    Generates a unique download key.
    """
    return 'user:drive#{}'.format(hash(request.user))


def redirect_uri(request):
    """
    Computes the appropriate redirect URI.
    """
    return request.build_absolute_uri(reverse('providers:google'))


def create_file(service, file_id, team_id):
    """
    Creates a file on the server (name collision possible).
    """
    try:
        file_details = service.files().get(fileId=file_id).execute()
    except errors.HttpError:
        # print('An error occurred: {}'.format(exc))
        raise

    # Make sure the directories exist
    file_dir = os.path.join(settings.BASE_DIR, '.team_files', str(team_id))
    if not os.path.exists(file_dir):
        os.makedirs(file_dir)

    # Create and return the file
    # Location: {BASE_DIR}/.team_files/{TEAM_ID}/{FILE_NAME}
    file_path = os.path.join(file_dir, file_details['name'])
    return open(file_path, 'wb')


def download_file(service, file_id, local_fd):
    """
    Downloads the file from Google Drive.
    """
    download_progress = None
    file_request = service.files().get_media(fileId=file_id)
    media_request = http.MediaIoBaseDownload(local_fd, file_request)
    while True:
        try:
            download_progress, done = media_request.next_chunk()
        except errors.HttpError:
            # print('An error occurred: {}'.format(exc))
            break
    if download_progress and download_progress.progress() >= 1.0:
        return True


def download(request, code):
    """
    Authorizes the application and downloads the file.
    """
    # Create the Google service
    flow = client.flow_from_clientsecrets(CLIENT_SECRETS, SCOPES)
    flow.step1_get_authorize_url(redirect_uri=redirect_uri(request))

    # Restore the request from the cache
    google_request = json.loads(cache_key(request))

    # Authorize with the service
    credentials = flow.step2_exchange(code)
    auth_http = credentials.authorize(httplib2.Http())
    service = discovery.build('drive', 'v3', http=auth_http)

    # Create the local file by name
    file_id = google_request['file_id']
    team_id = google_request['team_id']
    local_fd = create_file(service, file_id, team_id)

    # Download the file and close it
    if download_file(service, file_id, local_fd):
        File.objects.create(
            owner=request.user,
            parent_id=team_id,
            name=os.path.basename(local_fd.name),
            location=os.path.abspath(local_fd.name),
        )
    local_fd.close()
    return team_id


def authorize(request, google_request):
    """
    Redirects the user to Google's authorization page.
    """
    # Cache the request
    cache.set(cache_key(request), json.dumps(google_request))

    # Return the redirect URI
    flow = client.flow_from_clientsecrets(CLIENT_SECRETS, SCOPES)
    return flow.step1_get_authorize_url(redirect_uri=redirect_uri(request))


@require_logged_in
def index(request):
    """
    Processes OAuth connections with Google.
    """
    if request.method == 'GET':
        # Download the file from Google
        if 'code' in request.GET:
            code = request.GET['code']
            team_id = download(request, code)
            return redirect('team:files', team_id)
    raise Http404
