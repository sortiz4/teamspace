"""
Utility Tags and Filters

For more information on custom template tags and filters, see:
https://docs.djangoproject.com/en/1.10/howto/custom-template-tags/
"""
import markdown as md
import re
from django import template
from django.utils.html import strip_tags
from django.utils.safestring import mark_safe

register = template.Library()


@register.filter
def markdown(text):
    """
    Renders the given markdown string into HTML.
    """
    text = md.markdown(strip_tags(text), output_format='html5')
    text = re.sub('<blockquote>', '<blockquote class="ui segment">', text)
    text = re.sub('<code>', '<code class="ui tertiary segment">', text)
    return mark_safe(text)


@register.filter
def namespace(request, url_name):
    """
    Determines if the request matches the given namespace.
    """
    return url_name in request.resolver_match.namespace


@register.filter
def viewname(request, url_name):
    """
    Determines if the request matches the given view name.
    """
    return request.resolver_match.view_name == url_name
