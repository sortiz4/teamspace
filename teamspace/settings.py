"""
Django Settings

For more information on this file, see:
https://docs.djangoproject.com/en/1.10/topics/settings/

For the full list of settings and their values, see:
https://docs.djangoproject.com/en/1.10/ref/settings/
"""
import conf
import os

# The base directory of the project
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# SECURITY WARNING: Keep the secret key used in production secret
SECRET_KEY = ''

# SECURITY WARNING: Don't run with debug turned on in production
DEBUG = True
ALLOWED_HOSTS = ['*']
ROOT_URLCONF = 'teamspace.urls'
WSGI_APPLICATION = 'teamspace.wsgi.application'

# Application definition
INSTALLED_APPS = [
    'teamspace',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]
TEMPLATES = [{
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'DIRS': [os.path.join(BASE_DIR, 'templates')],
    'APP_DIRS': True,
    'OPTIONS': {
        'context_processors': [
            'django.template.context_processors.debug',
            'django.template.context_processors.request',
            'django.contrib.auth.context_processors.auth',
            'django.contrib.messages.context_processors.messages',
        ],
    },
}]

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'db.sqlite',
    }
}

# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators
AUTH_USER_MODEL = 'teamspace.User'
AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator'},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator'},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator'},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator'},
]
AUTHENTICATION_BACKENDS = [
    'teamspace.contrib.auth.backends.EmailBackend',
    'django.contrib.auth.backends.ModelBackend',
]
PROVIDERS = {
    'GOOGLE': {
        'CLIENT_ID': None,
        'APP_ID': None,
        'API_KEY': None,
        'SECRET': None,
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/
STATIC_URL = '/static/'
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, 'static'),
]

# Load environment settings
globals().update(conf.load('env'))
