"""
URL Configuration

For more information on URLs, see:
https://docs.djangoproject.com/en/1.10/topics/http/urls/
"""
from django.conf.urls import include
from django.conf.urls import url
from teamspace.views import dashboard
from teamspace.views import frontend
from teamspace.views import google
from teamspace.views import organization
from teamspace.views import team

handler403 = 'teamspace.views.error'
handler404 = 'teamspace.views.error'

urlpatterns = [
    # Frontend routes
    url(
        r'^',
        include(
            [
                url(r'^$', frontend.home, name='home'),
                url(r'^about/$', frontend.about, name='about'),
                url(r'^login/$', frontend.login, name='login'),
                url(r'^logout/$', frontend.logout, name='logout'),
                url(r'^signup/$', frontend.signup, name='signup'),
            ],
            namespace='frontend',
        ),
    ),

    # Dashboard routes
    url(
        r'^d/',
        include(
            [
                # Root dashboard routes
                url(r'^$', dashboard.home, name='home'),

                # Account management routes
                url(
                    r'^u/',
                    include(
                        [
                            url(
                                r'^edit/$',
                                dashboard.user_edit,
                                name='edit',
                            ),
                            url(
                                r'^password/$',
                                dashboard.user_password,
                                name='password',
                            ),
                        ],
                        namespace='user',
                    ),
                ),

                # Organization access/creation routes
                url(
                    r'^o/',
                    include(
                        [
                            url(
                                r'^create/$',
                                dashboard.orgs_create,
                                name='create',
                            ),
                            url(
                                r'^view/$',
                                dashboard.orgs_view,
                                name='view',
                            ),
                        ],
                        namespace='organization',
                    ),
                ),
            ],
            namespace='dashboard',
        ),
    ),

    # Organization routes
    url(
        r'^o/([0-9]+)/',
        include(
            [
                # Root organization routes
                url(r'^$', organization.home, name='home'),
                url(r'^delete/$', organization.delete, name='delete'),
                url(r'^leave/$', organization.leave, name='leave'),
                url(r'^users/$', organization.users, name='users'),

                # Organization management routes
                url(
                    r'^m/',
                    include(
                        [
                            url(
                                r'^edit/$',
                                organization.manage_edit,
                                name='edit',
                            ),
                            url(
                                r'^users/$',
                                organization.manage_users,
                                name='users',
                            ),
                        ],
                        namespace='manage',
                    ),
                ),

                # Team access/creation routes
                url(
                    r'^t/',
                    include(
                        [
                            url(
                                r'^create/$',
                                organization.teams_create,
                                name='create',
                            ),
                            url(
                                r'^view/$',
                                organization.teams_view,
                                name='view',
                            ),
                        ],
                        namespace='team',
                    ),
                ),
            ],
            namespace='organization',
        ),
    ),

    # Team routes
    url(
        r'^t/([0-9]+)/',
        include(
            [
                # Root team routes
                url(r'^$', team.home, name='home'),
                url(r'^chat/$', team.chat, name='chat'),
                url(r'^delete/$', team.delete, name='delete'),
                url(r'^events/$', team.events, name='events'),
                url(r'^files/$', team.files, name='files'),
                url(r'^leave/$', team.leave, name='leave'),
                url(r'^users/$', team.users, name='users'),

                # Team management routes
                url(
                    r'^m/',
                    include(
                        [
                            url(r'^edit/$', team.manage_edit, name='edit'),
                            url(r'^users/$', team.manage_users, name='users'),
                        ],
                        namespace='manage',
                    ),
                ),
            ],
            namespace='team',
        ),
    ),

    # Provider routes
    url(
        r'^providers/',
        include(
            [url(r'^google/$', google.index, name='google')],
            namespace='providers',
        ),
    ),
]
