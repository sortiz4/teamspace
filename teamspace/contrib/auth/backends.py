"""
Authentication Backends

For more information on authenticating users, see:
https://docs.djangoproject.com/en/1.10/topics/auth/default/#authenticating-users
"""
from django.contrib.auth.backends import ModelBackend
from teamspace.models import User


class EmailBackend(ModelBackend):
    """
    Custom authenticator using an email address.
    """

    def authenticate(self, email='', password='', **kwargs):
        try:
            user = User.objects.get(email=email)
            if user.check_password(password):
                return user
        except User.DoesNotExist:
            pass
        return None
