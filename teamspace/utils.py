from django.utils.html import strip_tags


def format_errors(errors):
    """
    Formats a dictionary of form errors as an unordered HTML list.
    """
    string = ['<ul class="list">']
    for field in errors:
        string.append('<li>{}</li>'.format(strip_tags(errors[field])))
    string.append('</ul>')
    return ''.join(string)
