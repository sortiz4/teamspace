# Teamspace
I made this web application for a software engineering class while I was an
undergraduate at UTSA back in 2016. It was my first Django-based project, so
please keep that in mind if you decide to take a look at the code. I've
reformatted the project and made a few changes.

## Requirements
- Python 3.5

## Setup
After cloning the project, create a virtual environment in the project
directory.

```sh
$ virtualenv virtualenv
```

Activate the virtual environment. On Windows, `activate` is under `scripts`.

```sh
$ source virtualenv/bin/activate
```

Install the project dependencies for your environment.

```sh
$ pip install -r requirements.txt
```

Add `env.yaml` to the `conf` module. These settings will be injected into the
`settings.py` module on startup and should reflect Django's official settings.

```yaml
DEBUG: True
SECRET_KEY: ''

PROVIDERS:
  GOOGLE:
    SECRET: ''
    APP_ID: ''
    API_KEY: ''
    CLIENT_ID: ''
```

Add `google.json` to the `conf` module. This can be downloaded from the Google
Developer Console and is required to upload files to the application server.

Migrate the project's database. This should be done whenever the models have
been changed or Django has been upgraded.

```sh
$ python manage.py makemigrations teamspace
$ python manage.py migrate
```

Run the application by executing `python manage.py runserver`.
