import os
import yaml

# The base configuration directory
BASE_DIR = os.path.dirname(__file__)


def load(module):
    """
    Loads the given configuration module.
    """
    path = '{}.yaml'.format(os.path.join(BASE_DIR, *module.split('.')))
    try:
        with open(path) as file:
            return yaml.safe_load(file)
    except FileNotFoundError:
        pass
    raise ImportError("No module named 'conf.{}'".format(module))
