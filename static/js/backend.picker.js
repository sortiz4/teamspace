// This application focuses on Google Drive
var scope = ['https://www.googleapis.com/auth/drive/'];

// These variables are modified by the event loop
var pickerApiLoaded = false;
var oauthToken;

// These variables are initialized by the template
var developerKey, clientId, appId;

/**
 * Initializes the picker event loop in the browser.
 */
function initPicker(developerKeyIn, clientIdIn, appIdIn) {
    developerKey = developerKeyIn;
    clientId = clientIdIn;
    appId = appIdIn;
}

/**
 * Authorizes and loads the Google Drive 'Picker'.
 */
function loadPicker() {
    gapi.load('auth', {
        callback: function () {
            gapi.auth.authorize(
                {
                    client_id: clientId,
                    immediate: false,
                    scope: scope,
                },
                function (authResult) {
                    if (authResult && !authResult.error) {
                        oauthToken = authResult['access_token'];
                        createPicker();
                    }
                },
            );
        },
    });
    gapi.load('picker', {
        callback: function () {
            pickerApiLoaded = true;
            createPicker();
        },
    });
}

/**
 * Creates the Google Drive 'Picker' window.
 */
function createPicker() {
    if (pickerApiLoaded && oauthToken) {
        var view = new google.picker.DocsView(google.picker.ViewId.DOCS);
        view.setMode(google.picker.DocsViewMode.GRID);
        var picker = (
            new google.picker.PickerBuilder()
                .enableFeature(google.picker.Feature.MULTISELECT_ENABLED)
                .setAppId(appId)
                .setOAuthToken(oauthToken)
                .addView(view)
                .setDeveloperKey(developerKey)
                .setCallback(pickerCallback)
                .build()
        );
        picker.setVisible(true);
    }
}

/**
 * Extracts the file key from the picker.
 */
function pickerCallback(data) {
    if (data.action === google.picker.Action.PICKED) {
        var fileId = data.docs[0].id;
        $('#result').text(fileId);
        uploadFile(fileId);
    }
}

/**
 * Submits the form.
 */
function uploadFile(fileId) {
    $('[name=file_id]').val(fileId);
    $('#hidden').submit();
}
