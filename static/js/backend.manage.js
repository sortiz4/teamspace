/**
 * Shows and modifies the delete-leave modal.
 */
function verify(link, action, scope) {
    var scopeTitle = null;
    var actionTitle = null;
    switch (scope) {
        case 'team':
            scopeTitle = 'Team';
            break;
        case 'organization':
            scopeTitle = 'Organization';
            break;
    }
    switch (action) {
        case 'delete':
            actionTitle = 'Delete';
            break;
        case 'leave':
            actionTitle = 'Leave';
            break;
    }
    function getTitle() {
        return actionTitle + ' ' + scopeTitle + '.';
    }
    function getDescription() {
        return 'Are you sure you want to ' + action + ' this ' + scope + '? This action cannot be undone.';
    }
    $('#verify-title').text(getTitle());
    $('#verify-description').text(getDescription());
    $('#verify-link').text(actionTitle).attr('href', link);
    $('#verify').modal('show');
}

/**
 * Removes the user from an organization or team.
 */
function remove(id) {
    $('[name=id]').val(id);
    $('#hidden').submit();
}
